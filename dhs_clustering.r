#================================================================================================================================
# MIT LICENCE
# Copyright (c) 2018, Amanda Styles
#  
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation 
#  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
#  and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#  
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
#   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
#   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
#   OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#================================================================================================================================
#
#  THIS IS THE MAIN SCRIPT THAT RUNS THE LCA AND CREATES THE MAPS - see ReadMe.md
#
#  It is recommended to run the steps as blocks of code ensuring data is ok before moving onto the next step
# 
# Required packages: dplyr, foreign, funModeling, rgdal, ggmap, poLCA, clustrd, factoextra, FactoMineR
## # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# Initial set up 
setwd("C:/Users/merzouki/Documents/gitlab_repositories/dhs_malawi_lca/") # Set this if necessary to ensure scripts can be accessed
basePath = 'C:/Users/merzouki/Documents/DHS_Malawi_Data/MW_2015-16_DHS_07172018_428_122569/' # Location of files - subdirectories can be set later if necessary
source('dhs_build_dataset.r')
source('dhs_helper_functions.r')
source('dhs_plotmaps.r')
source('dhs_lca.r')
set.seed(123)
## # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# BLOCK 1 - Set the gender (as 'F' or 'M')and build the dataset - optionally input the filename/pathname to create CSV file modeling
#
#
gender = 'M'# 'M' or "F"
# Set up the filenames and locations
DHS_F_file <- 'MWIR7HDT/MWIR7HFL.DTA'     # Female data
DHS_M_file <- 'MWMR7HDT/MWMR7HFL.DTA'     # Male data
HIV_file <- 'MWAR7ADT/MWAR7AFL.DTA'

#
# Default to Males as this is the smaller dataset
#
input_file <- DHS_M_file
gender_desc <- 'Male'
min_cluster <- 4
max_cluster <- 8

if (gender == 'F') {
  input_file <- DHS_F_file
  gender_desc <- 'Female'
  min_cluster <- 6
  max_cluster <- 15
}
#
# Call the routine that creates the dataset for clustering
#
DHS.main <- get_DHS_dta(gender,basePath,TRUE,input_file, HIV_file)  # called from dhs_build_dataset.r

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# BLOCK 2 -Set up the columns for the LCA clusterings
# This sets up the column names and the formula for the LCA that will used for clustering - modify a required
#
if (gender == "F"){
  subtitle = 'Age Union Location HIV Tested Working  Gender of Head Literacy'
  LCA_form <- cbind(Age_Band_Manual,   Union,   Location,   HIVTested,   Working,   Literacy,   HHead) ~ 1
  cols_to_use <- c('Age_Band_Manual', 'Union', 'Location', 'HIVTested', 'Working', 'Literacy', 'HHead')
} else {
  subtitle = 'Age AgeFirstSex Union Location HIVTested hasMediaAccess Working Literacy Gender of Head, BeatingsOK, CondomOK '
  LCA_form <- cbind(Age_Band_Manual, AgeFirstSex, Union, Location, HIVTested, hasMediaAccess, Working, Literacy, HHead, BeatingsOK, CondomOK ) ~ 1
  cols_to_use <- c('Age_Band_Manual', 'AgeFirstSex', 'Union', 'Location', 'HIVTested', 'hasMediaAccess', 'Working', 'Literacy',  'HHead','BeatingsOK', 'CondomOK')
}

# subtitle = 'Age, AgeFirstSex, Literacy, Working, Union, hasMediaAccess, Location, Gender of HHead, AIDS Knowledge OK, BeatingsOK, CondomOK, Religion, HIV Stigma, HIVTested '
# LCA_form <- cbind(Age_Band_Manual, AgeFirstSex, Literacy, Working, Union, hasMediaAccess,  Location, HHead, AIDSKnowledgeOK,   BeatingsOK, CondomOK, Religion, HIVStigma, HIVTested ) ~ 1
# cols_to_use <- c('Age_Band_Manual', 'AgeFirstSex' , 'Literacy', 'Working', 'Union', 'hasMediaAccess',   'Location', 'HHead', 'AIDSKnowledgeOK' ,'BeatingsOK', 'CondomOK', 'Religion', 'HIVStigma', 'HIVTested')

#
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # '
#
# BLOCK 3 - This step is optional if the best cluster is already known 
# This runs LCA over a varying number of clusters and prints the BICs for each clsuter
#
#
# dta.LCA <- run_LCA(DHS.main, cols_to_use, LCA_form, min_clusters = min_cluster, max_clusters = max_cluster) # called from dhs_lca.r
# plt <- ggplot(dta.LCA, aes(x=cluster_size, y=BIC))
# plt <- plt + geom_point() + geom_line()
# plt <- plt + labs(title = paste('BIC for each cluster -', gender_desc, 'Repsondents'), subtitle=subtitle)
# plt <- plt + xlab("Cluster") + ylab("BIC")
# plt <- plt + theme_classic()
# plt
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# BLOCK 4 - Get best cluster membership.
# Update the best_cluster from the BIC plot
#
best_cluster <- 6
if (gender == 'F') {
  best_cluster <- 9
}
LCA.Clusters <- get_clusters_LCA(DHS.main, cols_to_use, LCA_form, best_cluster) # called from dhs_lca.r
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# BLOCK 5 - OPTIONAL
#
# Do the Dimensionality Reduction if needed
#
#
# This uses clusmca from the clustrd
CACluster(DHS.main, best_cluster, LCA.Clusters$predclass, cols_to_use, title = 'LCA Male Respondents', subtitle = subtitle)
#
# This uses MCA using facto extra and associated plotting package
library("factoextra")
library("FactoMineR")
dta <- as.data.frame(unclass(DHS.main))
dta.cluster <- dta[ , (names(dta) %in% cols_to_use)]
res.mca <- MCA(dta.cluster)
# fviz(res.mca, addlabels = TRUE)
fviz_screeplot(res.mca, addlabels = TRUE)
fviz_mca_biplot(res.mca, ggtheme = theme_minimal())
fviz_mca_var(res.mca, col.var = 'contrib', ggtheme = theme_minimal())

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# BLOCK 6
#
# Prints the clusters and colours the HIV variation
# For countries other than Malawi the regions will need updating in the routine dhs_plotmaps::createcountrymap() 
#
# Run this first as it links the main data with the geo data
dta.cluster <- as.data.frame(unclass(DHS.main))
geoclusters <- getclusterlocation (dta.cluster , LCA.Clusters$predclass, basePath, file = 'MWGE7AFL/MWGE7AFL.dbf')

# Get the shape file and output the clusters as bubbles on the map  
filepath <- paste0(basePath, "MAA-level_1")
shpfilename <- "MAA-level_1"
getclusterplots(geoclusters, best_cluster, filepath, shpfilename, gender)           # see dhs_plotmaps.r

# This outputs the breakdown to disk. There is a barchart created in each region
# Palette should equal the number of clusters
cbbPalette <- c("lightskyblue1", "hotpink1", "indianred2", "darkorange1", "thistle2", "darkseagreen2", "lightyellow4", "lightsteelblue3")
# This is the shape file with the districts from https://geonode.wfp.org/layers/geonode%3Amwi_bnd_admin2
filepath <- paste0(basePath, "mwi_bnd_admin2")
# print_malawi_district_clusters(DHS.main, LCA.Clusters$predclass, cbbPalette, gender, basePath, filepath)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# BLOCK 7 - Useful misc functions 
#
# ---------------------------------------------------------------------------------------------------
# FEATURE CORRELATION
# This outputs a heatmap of the feature correlations
plot_corelations(DHS.main,cols_to_use,gender)

# ---------------------------------------------------------------------------------------------------
# EXPLORATORY DATA ANALYSIS - Plot barcharts
# Sum vec is the totals for each cluster - 99999 is a dummy
#
plot_feature_barcharts(DHS.main, cols_to_use)

# Use ggplot to view one feature at a time
col_to_view <- 'AgeFirstSex'
dta.plot <- as.data.frame(table(DHS.main[,col_to_view]))
dta.plot$name <- col_to_view
colnames(dta.plot) <- c('Category','Count','Name')

title <- col_to_view
plt <- ggplot(dta.plot, aes(x=Category, y=Count))
plt <- plt + labs(title=title, subtitle=paste(gender_desc, "Respondents"))#, caption=caption)
plt <- plt + geom_bar(stat = "identity",width = 0.1, fill="steelblue", position = 'dodge')
plt <- plt + xlab("Category") + ylab("No of Respondents")
plt <- plt + theme_minimal()
plt
# ---------------------------------------------------------------------------------------------------
# FEATURE ELIMINATION
# Get the sum of the variance for the best cluster. This outputs to the console the 
# sum of the variances of the categories for each feature. From this
get_variances(LCA.Clusters, cols_to_use)

# ---------------------------------------------------------------------------------------------------
# OUTPUT FOR VISUALISATION
# Summarise the data and output to disk for manipulation in Excel/Tableau
dta.clustered <- cbind(DHS.main, LCA.Clusters$predclass)
output_cluster_to_disk(dta.clustered, paste0(basePath,gender,'_','Results','.csv'))

# This is a list of all columns that can be used above
# All cols for copying as required
#
subtitle = 'Age, AgeFirstSex, Working, Literacy, Working, Union, hasMediaAccess, Location, Gender of HHead, AIDS Knowledge OK, BeatingsOK, CondomOK, Religion, HIV Stigma, HIVTested '
LCA_form <- cbind(Age_Band_Manual, AgeFirstSex, Literacy, Working, Union, hasMediaAccess,  Location, HHead, AIDSKnowledgeOK,   BeatingsOK, CondomOK, Religion, HIVStigma, HIVTested ) ~ 1
cols_to_use <- c('Age_Band_Manual', 'AgeFirstSex' , 'Literacy', 'Working', 'Union', 'hasMediaAccess',   'Location', 'HHead', 'AIDSKnowledgeOK' ,'BeatingsOK', 'CondomOK', 'Religion', 'HIVStigma', 'HIVTested')
