#================================================================================================================================
# MIT LICENCE
# Copyright (c) 2018, Amanda Styles
#  
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation 
#  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
#  and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#  
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
#   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
#   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
#   OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#================================================================================================================================
library(ggmap)
library(dplyr)
library(rgdal)
library(png)
#================================================================================================================================
#  
#   This is the main interface that has a couple of helper functions see below
#    to print the country map
#
# INPUT PARAMETERS
#  HIVClusters - The dataset with the cluster membership for respondents with known HIV status
#  no_of_clusters -  cluster size
#  tocsv - set to TRUE to print the output file to disk
#  DHS_file - the subdirectory and filename location of the main datafile
#  filepath - the directory location of the .shp file
#  filename - the name of the shape file
#  gender - gender as 'M' or 'F' - used for the title. Defaults to Male
#
# OUTPUT PARAMETERS
#  Returns a list of plots - one for each cluster
#
getclusterplots <- function (HIVclusters, no_of_clusters, filepath, filename, gender) {
  #
  # Get the country map from the shap file. Ensure the shape file doesn't have the .shp extension and 
  # the file path doesn't have the trainling '/'
  #
  # http://geoportal.rcmrd.org/layers/servir%3Amalawi_adm3
  # https://geonode.wfp.org/layers/geonode%3Amwi_bnd_admin2
  countrymap <- createcountrymap(filepath, filename)
  #
  # Set up the data set with the cluster longitude and latititudes. That should have been dome previously
  #
  plts <- list()
  cols_to_keep = c('clusterno', 'HIVStatus', 'cluster', 'LATNUM', 'LONGNUM')
  
  HIVclusters_red <- HIVclusters[ , (names(HIVclusters) %in% cols_to_keep)]
  #
  # Thsi gives the dataset as a unique value for each row.
  #
  HIVclusters_red$longlat <- paste(as.character(HIVclusters_red$LATNUM),as.character(HIVclusters_red$LONGNUM))
  #
  # Print a map for each cluster
  # 
  title_gender = 'Males'
  if (gender == 'F') {
    title_gender = 'Females'
  } 

  for (i in seq(1, no_of_clusters)) {
    #
    # Pass each cluster to the printing routine
    #
    plts[[i]] <- printmap(countrymap, subset(HIVclusters_red, HIVclusters_red$cluster == i), i, title_gender)
  }
  return (plts)
}
#
#================================================================================================================================
#   These is a helper function for the main  function that creates the
#   country map from.shp file
# Check the number of 'id' using table(countrymap.df$id) - the code below will need changing for different countries
# Update so that all ids have a colour set if the regions require colours
#
#
# INPUT PARAMETERS
#  filepath - the directory location of the .shp file
#  filename - the name of the shape file
#
# OUTPUT PARAMETERS
#  Returns the shape file as a dataframe with each region in a separate colour
#
createcountrymap <- function(filepath, filename) {
  #
  # Read the shape file using rgdal
  #
  countrymap.shp <-readOGR(dsn = path.expand(filepath), layer = filename)
  #
  # Convert the shape data to a dataframe
  #
  countrymap.df <- fortify(countrymap.shp)
  #
  # Analysis of the dataframe has shown which ids relate to which region
  #
  for (i in seq(0,12)) {
    if (i <= 3) {
      countrymap.df[countrymap.df['id'] == i , 'colorid'] <- 'sienna1'
      countrymap.df[countrymap.df['id'] == i , 'alphaid'] <- 0.9
      countrymap.df[countrymap.df['id'] == i , 'Region'] <- 1
    }
    
    if (i > 3 & i <= 6) {
      countrymap.df[countrymap.df['id'] == i , 'colorid'] <- 'thistle'
      countrymap.df[countrymap.df['id'] == i , 'alphaid'] <- 0.9
      countrymap.df[countrymap.df['id'] == i , 'Region'] <- 2
    }
    
    if (i > 6 & i <= 9) {
      countrymap.df[countrymap.df['id'] == i , 'colorid'] <- 'khaki3'
      countrymap.df[countrymap.df['id'] == i , 'alphaid'] <- 0.9
      countrymap.df[countrymap.df['id'] == i , 'Region'] <- 3
    }  
    
    if (i > 9) {
      countrymap.df[countrymap.df['id'] == i , 'colorid'] <- 'blue1'
      countrymap.df[countrymap.df['id'] == i , 'alphaid'] <- 0.1
    }  
  }

  return(countrymap.df)
}
#================================================================================================================================
#  
#   This does the ggplot to create the plot
#
#
# INPUT PARAMETERS
#  countrymap - the .shp file as a dataframe
#  clust - the cluster data - requires the HIV status flag
#  clust_no - the number of the cluster 
#  title_gender - title used for printing on the plot
#
# OUTPUT PARAMETERS
#  Returns the shape file as a dataframe with each region in a separate colour
#
printmap <- function(countrymap, clust, clust_no, title_gender) {
  
  #
  # This sets up the legend. It include the unknown cases but this is printed
  #
  pos_cases <- paste('Positive Cases - ',length(clust$HIVStatus[clust$HIVStatus == 1]))
  neg_cases <- paste('Negative Cases - ',length(clust$HIVStatus[clust$HIVStatus == 0]))  
  UK_cases <- paste('Unknown Cases - ',length(clust$HIVStatus[clust$HIVStatus == 8]))  
  
  # Just get the postive and negative cases
  clust <- subset(clust, HIVStatus == 1 | HIVStatus == 0)
  #
  # For this cluster summarise the data so we have a count of 
  # respondents for each location
  #
  plt_clust <- count(clust, longlat, HIVStatus, LATNUM, LONGNUM)
  #
  # Use the standard ggplot to ouptut the map
  #  
  plt <- ggplot(countrymap)
  plt <- plt + geom_polygon(aes(long,lat,group=group), fill = countrymap$colorid, alpha = countrymap$alphaid)
  plt <- plt + geom_path(aes(long,lat,group=group), color = 'gray85')
  plt <- plt + coord_equal() 
  plt <- plt + geom_point(data = plt_clust,  alpha= 0.55, aes( x = LONGNUM, y=LATNUM, colour = HIVStatus,  size = factor(n)))
  plt <- plt + scale_colour_manual(name = "HIV Status", values =c('springgreen1','red1'), labels = c(neg_cases, pos_cases))
  plt <- plt + labs(size="Point Size Count") 
  plt <- plt + xlab(" ") + ylab(" ") +  ggtitle(paste("HIV Spatial Variation:", title_gender,"- class",clust_no))
  return (plt)
  
}
#================================================================================================================================
#  
#   This prints a map for each region with the region highlighted and the bar chart for the region
#
# INPUT DATA
#  DHS.dta - is all the data from teh DHS that has been processed
#  clusterdata - has the cluster memebership
#  cbbPalette - are the colours to use
#  gender - 'Male' or 'Female' - used for rpinting report titles
#  basePath and filepath - give the output location for the files
#
# OUTPUT DATA
#  Writes png files to disk for each region
print_malawi_district_clusters <- function(DHS.dta, clusterdata, cbbPalette, gender, basePath, filepath) {
  #
  # Get the shape file data and convert to a dataframe
  #
  countrymap.shp <-readOGR(dsn = path.expand(filepath))
  countrymap <- fortify(countrymap.shp)
  
  gender_desc <- 'Female'
  if (gender == 'M') {
    gender_desc <- 'Male'
  }
  #
  # Output the district map and the barcharts 
  #
  for (i in seq(0,28)) {
    #
    #Colour the district to ggplot colour 'sienna1'
    # 
    countrymap['colorid'] <- 'honeydew4'
    countrymap[countrymap['id'] == i, 'colorid'] <- 'sienna1'
    countrymap[countrymap['id'] == 22, 'colorid'] <- 'gray100'  # This is the under National Administration
    
    plt <- ggplot(countrymap)
    plt <- plt + geom_polygon(aes(long,lat,group=group), fill = countrymap$colorid)
    plt <- plt + geom_path(aes(long,lat,group=group), color = 'gray100')
    plt <- plt + coord_equal()
    plt <- plt + xlab(" ") + ylab(" ")
    plt <- plt + theme_void()
    #
    # Set up a png plot
    #
    png_plot <- paste0(basePath,'malawi_',i,'.png')
    #
    # Save the image to a plot 
    #
    ggsave(file=png_plot, plot=plt, width=50, height=100, units="mm", type ="cairo-png", bg = "transparent")
    
  }
  #
  # Initialise the regions and their ids in the shape file
  #
  all_regions = c('Dedza','Dowa', 'Kasungu','Lilongwe', 'Mchinji','Nkhotakota','Ntchisi','Salima','Chitipa',
                 'Karonga','Rumphi' ,'Blantyre','Chikwawa','Chiradzulu','Nsanje','Thyolo','Zomba', 'Likoma', 'Nkhata Bay', 
                 'Balaka', 'Machinga', 'Phalombe','Neno' , 'Mulanje',
                   'Ntcheu','Mzimba','Mangochi','Mwanza')
  
  all_region_ids = c(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,23,24,25,26,27,28)
  df.region_names <- data_frame(all_regions,all_region_ids)
  
  #
  # Combine the raw data with the cluster membership
  #
  dta.clustered <- cbind(DHS.dta, clusterdata)
  # Chi-sq test
  #chi_out <- chisq.test(table(dta.clustered$clusterdata,dta.clustered$region_name))
  dta.region.cluster <- as.data.frame(table(dta.clustered$clusterdata,dta.clustered$region_name))
  names(dta.region.cluster) = c('Cluster', 'Admin_District', 'Count')
  #
  # This routine outputs a barchart for each district 
  #
  for (row in 1:nrow(df.region_names)) {
    #
    # Get the district
    #
    region_id <- df.region_names[row, "all_region_ids"]
    region_name <- df.region_names[row, "all_regions"]
    dta.region <- subset(dta.region.cluster, dta.region.cluster['Admin_District'] == as.character(df.region_names[row, "all_regions"]))
    #
    # Get the map with the district highlighted so this can be put into the top right corner
    #
    region_png <- readPNG(paste0(basePath,'malawi_',region_id,'.png'))
    
    plt <- ggplot(dta.region, aes(x=reorder(Cluster, -Count, sum), y=Count, fill = Cluster))
    plt <- plt + scale_fill_manual(values=cbbPalette)
    plt <- plt + labs(title=paste("Cluster Membership in", region_name), subtitle=paste(gender_desc, "Respondents"))
    plt <- plt + geom_bar(stat = "identity")
    plt <- plt + xlab("Cluster Membership") + ylab("Number of Respondents") 
    
    if (gender == 'M') {
      plt <- plt + annotation_raster(region_png, ymin = 50,ymax= 130,xmin = 4.1,xmax = 5.3)   #Male
      plt <- plt + scale_y_continuous(breaks=seq(0,130,10), limits = c(0,130))
    } else {
      plt <- plt + annotation_raster(region_png, ymin = 200,ymax= 450,xmin = 6.7,xmax = 8.3)    #Female
      plt <- plt + scale_y_continuous(breaks=seq(0,450,50), limits = c(0,450))
    }
    
    plt <- plt + theme(panel.background = element_blank(), axis.line = element_line(colour = "gray"), legend.position="none")
    png_plot <- paste0(basePath,'malawi_region',region_id,'_', region_name,'_',gender,'.png')
    ggsave(file=png_plot, plot=plt, type ="cairo-png", bg = "transparent")
  }
}
############################################################################################################################




