#================================================================================================================================
# MIT LICENCE
# Copyright (c) 2018, Amanda Styles
#  
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation 
#  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
#  and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#  
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
#   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
#   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
#   OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#================================================================================================================================
library(reshape2)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# Function to to do proportional test against all clusters
#
# INPUT PARAMETERS
#  dta - The dataset with the cluster membership for respondents with known HIV status
#  no_of_clusters -  cluster size
#
# OUTPUT PARAMETERS
#  Prints to the console the two sided prop test and the assoicated p.value
cluster_proptest <- function(dta, no_of_clusters) {
  
  for (i in seq(1:no_of_clusters)) {
    x <- c(length((dta$HIVStatus[(dta$HIVStatus == 1) & (dta$cluster == i)])), length(dta$HIVStatus[(dta$HIVStatus == 0)  & (dta$cluster == i)]))
    n <- c(length(dta$HIVStatus[dta$HIVStatus == 1]), length(dta$HIVStatus[dta$HIVStatus == 0]))
    result <- prop.test(x, n, alternative = 'two.sided')
    print(paste('Class', i))
    print(paste('pvalue:',result$p.value,'proportions',x))

  }
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# Function to visualise the clusters
#
#
visualisecluster <- function(df, maincolname, cluster, title = '', caption = 'DHS 2016', gender = 'Female', plt_clust = 999) {

  if (length(title) <= 1) {
    chart_title = maincolname
  }
  
  df.plot <- cbind(df, cluster)
  names(df.plot)[length(names(df.plot))] <- "clusterno" 
  
  
  dta.out <- as.data.frame(table(df.plot[[maincolname]], cluster))
  colnames(dta.out) <- c(maincolname,'Cluster','Count')
  
  if (plt_clust < 999) {
    dta.out <- subset(dta.out, Cluster == plt_clust)
  }
  
  
  theme_set(theme_bw())
  plt <- ggplot(dta.out, aes(x=Cluster, y=Count, total, fill=dta.out[[maincolname]]))
  plt <- plt + labs(title = chart_title, subtitle=paste(gender, "Respondents"), caption=caption)
  plt <- plt + geom_bar(stat = "identity")
  plt <- plt + scale_y_continuous(breaks=seq(0,25000,1000))
  plt <- plt + xlab("Cluster") + ylab("No of Respondents")
  plt <- plt + scale_fill_discrete(name = maincolname)
  return (plt)
  
}
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# MCA clustering - uses the function clusmca in package clustrd
#
# INPUT PARAMETERS
#  dta - The dataset with the cluster membership for respondents with known HIV status
#  no_of_clusters -  cluster size
#  clusters - cluster membership
#  title - title for plot
#  subtitle - subtitle for plot
#  caption - caption for the plot
#
# OUTPUT
#  Plot of the MCA analysis
#
CACluster <- function(dta, no_clusters, clusters, cols_to_use, title = ' ', subtitle = ' ', caption = 'DHS 2016') {
  
  dta <- as.data.frame(unclass(dta))
  dta.cluster <- dta[ , (names(dta) %in% cols_to_use)]
  
  outclusCA = clusmca(dta.cluster, no_clusters, 2, method = "clusCA", nstart = 10)
  
  df.CAplot <- cbind(as.data.frame(outclusCA$obscoord), clusters)
  colnames(df.CAplot) <- c('Dim_1','Dim_2','clusterno')
  df.CAplot$clusterno <- as.factor(df.CAplot$clusterno)
  
  theme_set(theme_bw())
  plt <- ggplot(aes(x = Dim_1, y = Dim_2), data = df.CAplot) + geom_point(aes(color = clusterno))
  plt <- plt + geom_point(aes(color = clusterno)) 
  plt <- plt + labs(title=paste("MCA Plot - ",title), subtitle=subtitle, caption=caption, color = 'Cluster')
  plt <- plt + xlab("Dimension 1") + ylab("Dimension 2")
  plt <- plt + scale_fill_discrete(name = 'Cluster Number')
  
  return (plt)
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# Get the cluster variances for each feature. 
#
# INPUT PARAMETERS
#  LCAClustResults - Output from LCA cluster poLCA::poLCA
#  cols_var -  colums to calculate the variance
#
# OUTPUT
#  Prints the sum of the variance to the console 
#
get_variances <- function(LCAClustResults, cols_var) {
  
  #
  # Initialise the vectors
  #
  col_name <- character(length(cols_var))
  col_var <- numeric(length(cols_var))
  index <- 0
  #
  # Get the probabilities for each class membership
  #
  probs <- as.data.frame(LCAClustResults$probs)
  #
  # Read through each column
  #
  for (each_col in cols_to_use) {
    #
    # From the probabilities get the column probabilities and 
    # transpose before converting to dataframe
    #
    cluster <- probs[,grepl(patt=each_col, names(probs))]
    cluster_t <- as.data.frame(t(cluster))
    #
    # Apply the variance using the R var function
    #
    variance_cluster <- sum(apply(cluster_t, 1, var))
    index <- index + 1
    #
    # Store the output in a list
    #
    col_name[index] <- each_col
    col_var[index] <- variance_cluster
    
  }
  #
  # Create a dataframe of the variances and return to the calling program
  #
  dta.var <- data.frame(col_name, col_var, stringsAsFactors=FALSE)
  return (dta.var[order(col_var),])
}  
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# Summarise clustering results and output the clustering results to disk. Used for exporting data 
# to Excel for bar charts. This give a count and precentage of each cluster for a region
#
# INPUT PARAMETERS
#  dta.clustered - data with the cluster membership
#  pathfilename -  output location for the file
#
# OUTPUT
#  Writes the summarised cluster information to disk
#
output_cluster_to_disk <-function(dta.clustered, pathfilename) {
  
  # Create a dataframe that has a count and a percentage of the respondents in each cluster for each region.
  dta.region.cluster <- as.data.frame(table(dta.clustered$LCA.Clusters,dta.clustered$region_name))
  names(dta.region.cluster) = c('Cluster', 'Admin_District', 'Count')
  dta.region.clusterperc <- group_by(dta.region.cluster, dta.region.cluster$Admin_District) %>% mutate(percent = round((Count/sum(Count) * 100),2))
  dta.region.clusterperc <- as.data.frame(dta.region.clusterperc)
  
  # Output this to a csv file
  write.csv(dta.region.clusterperc, file = pathfilename)
}
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# Plot the correlations of a set of features as a heatmap of the p.values
#
#
# INPUT PARAMETERS
#  df - the dataset
#  cols_to_use -  list of strings with the columns to analyse
#  gender - 'M' or 'F' used for the title
#  p.val - p value for the cutoff
#
# OUTPUT
#  Writes the summarised cluster information to disk
#
plot_corelations <- function(df, cols_to_use, gender, pval_cut_off = 0.05) {
  
  gender_desc <- 'Males'
  if (gender == 'F') {
    gender_desc <- 'Females'
  }
  
  i <- 1
  index <- 0
  no_of_cols <- length(cols_to_use)
  
  # Create a dataframe that holds teh correlation for each combination of pair of columns
  # This allows for the square iumage teh duplicates the values.
  vector_size <- no_of_cols * no_of_cols
  
  x_col <- character(length(vector_size))
  y_col <- character(length(vector_size))
  chi_val <- numeric(length(vector_size))
  p_val <- numeric(length(vector_size))
  
  for (each_col in cols_to_use) {

    start_col <- i + 1
    list_to_check <- cols_to_use[start_col:no_of_cols]
    
    for (each_test_col in cols_to_use) {
      
      chi_results = chisq.test(table(df[,each_col], df[,each_test_col]))
      
      index <- index + 1
      x_col[index] <- each_col
      y_col[index] <- each_test_col
      chi_val[index] <- chi_results$statistic
      p_val[index] <- chi_results$p.value
    }
    
    i <- i + 1
  }
  
  # Check for the required cut-off
  dta.chisq <- data.frame(x_col, y_col, chi_val, p_val, stringsAsFactors=FALSE)
  dta.chisq[dta.chisq$p_val <= pval_cut_off, 'p_val' ] <- '<= 0.05'
  dta.chisq[dta.chisq$p_val > pval_cut_off, 'p_val' ] <- '> 0.05'
  dta.chisq$p_val <- as.factor(dta.chisq$p_val)
  
  # PLot teh heatmap and return to the calling function
  plt <- ggplot(dta.chisq, aes(x_col, y_col)) 
  plt <- plt + labs(title=paste("Correlation of Features -", gender_desc), caption = 'Malawi Demographic and Health Survey 2016 [Dataset]')
  plt <- plt + geom_tile(aes(fill = p_val), colour = 'white')
  plt <- plt + theme(axis.text.x = element_text(angle = 90, hjust = 1))
  plt <- plt + xlab(" ") + ylab(" ")
  plt <- plt + scale_fill_manual((name ="P value"), values = c("skyblue3", "dodgerblue1"))
  
  return (plt)
  
}
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# Plots of the features as counts of the different categories.
# This is a quick set ofg polots for looking at feature variations
#
##
# INPUT PARAMETERS
#  df - the dataset
#  cols_to_use -  list of strings with the columns to analyse
#
# OUTPUT
#  A plot for each of the columns
#
plot_feature_barcharts <- function(df, cols_to_use) {
  
  samp.IR <- df
  samp.IR <- as.data.frame(unclass(samp.IR))
  
  dta.cluster <- samp.IR[ , (names(samp.IR) %in% cols_to_use)]
  
  for (i in 1:length(cols_to_use)) {
    
    plot(dta.cluster[,i], main = colnames(dta.cluster)[i], ylab = 'Count', las = 2)
    
  }

}
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# Plots of the features as counts of the different categories.
# This returns a list of ggplots to the calling function 
#
plot_barchart <- function(input.data, col_names, gender, title = ' ') {

  caption = 'Malawi Demographic and Health Survey 2016 [Dataset]'
  
  gender_desc <- 'Male'
  if (gender == 'F') {
    gender_desc <- 'Female'
  }
  
  plts <- list(length(col_names))
  i <- 0
  
  for (each_col in col_names){
    dta.plot <- as.data.frame(table(input.data[,each_col]))
    dta.plot$name <- each_col
    colnames(dta.plot) <- c('Category','Count','Name')
    
    i = i + 1
    
    title <- each_col
    plt <- ggplot(dta.plot, aes(x=Category, y=Count))
    plt <- plt + labs(title=title, subtitle=paste(gender_desc, "Respondents"), caption=caption)
    plt <- plt + geom_bar(stat = "identity",width = 0.1, fill="steelblue", position = 'dodge')
    # plt <- plt + scale_y_continuous(breaks=seq(0,25000,1000))
    plt <- plt + xlab("Category") + ylab("No of Respondents")
    plt <- plt + theme_minimal()
    
    plts[i] <- plt
  }
  
  return(plts)
}  
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# Prints the chi sq statistic a p.value for each cluater for each region when compared to the total
#
# sum_vec - are the totals for each cluster
print_chi <- function(dta, sum_vec) {
  
  dta.region.cluster <- as.data.frame(table(dta$LCA.Clusters,dta$region_name))
  names(dta.region.cluster) = c('Cluster', 'Admin_District', 'Count')
  dta.region.clusterperc <- group_by(dta.region.cluster, dta.region.cluster$Admin_District) %>% mutate(percent = round((Count/sum(Count) * 100),2))
  dta.region.clusterperc <- as.data.frame(dta.region.clusterperc)
  
  all_plots <- list()
  all_regions = c('Dedza','Dowa', 'Kasungu','Lilongwe', 'Mchinji','Nkhotakota','Ntchisi','Salima','Chitipa',
                   'Karonga','Rumphi' ,'Blantyre','Chikwawa','Chiradzulu','Nsanje','Thyolo','Zomba', 'Likoma', 'Nkhata Bay', 
                   'Balaka', 'Machinga', 'Phalombe','Neno' , 'Mulanje',
                   'Ntcheu','Mzimba','Mangochi','Mwanza')
   
   i <- 0 
   for (each_region in all_regions) {
     
     i <- i + 1
   
     dta.region <- subset(dta.region.clusterperc, dta.region.clusterperc['Admin_District'] == each_region)
     df <- melt(dta.region)
     df <- subset(df, variable != 'percent')
     df <- dcast(df, Admin_District ~ Cluster + variable)
     df <- rbind(df, sum_vec)
     df <- subset(df, select=-c(Admin_District))
     chi <- chisq.test(as.matrix(df))
     print (paste(each_region, chi$statistic, chi$p.value))
     
   }
}
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
#
# Link main dta to location data
#
#
##
# INPUT PARAMETERS
#  clusterdata - the main dataset
#  algorithm_clusters -  cluster membership - probably from poLCA::poLCA$predclass
#  basePath and file - sets the location for the DHS geo file
#
# OUTPUT
#  A plot for each of the columns
#
getclusterlocation <- function(clusterdata, algorithm_clusters, basePath, file = 'MWGE71FL/MWGE71FL.dbf') {
  
  dta.clustered <- clusterdata
  cluster <- algorithm_clusters
  
  dta.clustered <- cbind(dta.clustered, cluster)
  
  filepath = paste0(basePath, file)
  cluster_locations <- read.dbf(filepath, as.is = FALSE)
  
  names(cluster_locations)[names(cluster_locations) == 'DHSCLUST'] <- 'clusterno'
  
  cols_locationjoin <- c('clusterno')
  dta.clustered <- merge(dta.clustered, cluster_locations, cols_locationjoin)

  return (dta.clustered)
}