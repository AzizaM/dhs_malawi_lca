#================================================================================================================================
# MIT LICENCE
# Copyright (c) 2018, Amanda Styles
#  
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation 
#  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
#  and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#  
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
#   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
#   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
#   OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#================================================================================================================================
# -------------------------------------------------------------------------------------------
# 
#  Data Preparation 
#  1) Removed missing rows
#  2) Bucketise the Age at First Sex but dropped those who have not had sex
#  3) Included weigths for sampling
#  4) Set Manual Age Bands
#  5) Aggregate several features
# --------------------------------------------------------------------------------------------
# 
library(dplyr) 
library(foreign)
library(funModeling)
library(mice)
# INPUT PARAMETERS
#  gender - 'M' for males and 'F' for females
#  basePath - the location of the data
#  tocsv - set to TRUE to print the output file to disk
#  DHS_file - the subdirectory and filename location of the main datafile
#  HIV_file - the subdirectory and the filename location of the HIV file
#
# OUTPUT PARAMETERS
#  Returns the pre-processed data
#
get_DHS_dta <- function(gender, basePath, tocsv = FALSE, DHS_file, HIV_file) {
  #
  # The same data is required for both male and female so access the relevant files and field names and
  # put into variables for later use
  #
  if (gender == 'F') {
    # Read in stata file - don't use factors as this can cause issues
    IR_dta <- read.dta(file=paste0(basePath, DHS_file), convert.factors = FALSE)
    
    # Set up column names to read in but translate the recode
    clusterno <- 'v001'
    hholdno <- 'v002'
    rplno <- 'v003'
    sample_weight <- 'v005'
    Act_Age <- 'v012'
    Age <- 'v013'        # Included for manual banding of ages
    region_no <- 'v023'
    Location <- 'v025'
    HHead <- 'v151'
    Literacy <- 'v155'
    hasMediaAccess_Paper <- 'v157'
    hasMediaAccess_Radio <- 'v158'
    hasMediaAccess_TV <- 'v159'
    AgeFirstSex <- 'v531'
    Working <- 'v731'
    Beating_GoingOut <- 'v744a'
    Beating_Child_Neglect <- 'v744b'
    Beating_Argues <- 'v744c'
    Beating_NoSex <- 'v744d'
    Beating_BurntFood <- 'v744e'
    Union <- 'v502'
    Heard_of_AIDS <- 'v751'
    AIDS_fromMosquitos <- 'v754jp'
    AIDS_fromFood <- 'v754wp'
    AIDS_inAppearance <- 'v756'
    CondomOK <- 'v822'
    HIVTested <- 'v781'
    Religion <- 'v130'
    Food_Purchase <- 'v825'
  } else {
    
    # Read in stata file - don't use factors as this can cause issues
    IR_dta <- read.dta(file=paste0(basePath, DHS_file), convert.factors = FALSE)
    
    clusterno <- 'mv001'
    hholdno <- 'mv002'
    rplno <- 'mv003'
    sample_weight <- 'mv005'
    Act_Age <- 'mv012'                     # Included for manual banding of ages
    Age <- 'mv013'
    region_no <- 'mv023'
    Location <- 'mv025'
    HHead <- 'mv151'
    Literacy <- 'mv155'
    hasMediaAccess_Paper <- 'mv157'
    hasMediaAccess_Radio <- 'mv158'
    hasMediaAccess_TV <- 'mv159'
    AgeFirstSex <- 'mv531'
    Working <- 'mv731'
    Beating_GoingOut <- 'mv744a'
    Beating_Child_Neglect <- 'mv744b'
    Beating_Argues <- 'mv744c'
    Beating_NoSex <- 'mv744d'
    Beating_BurntFood <- 'mv744e'
    Union <- 'mv502'
    Heard_of_AIDS <- 'mv751'
    AIDS_fromMosquitos <- 'mv754jp'
    AIDS_fromFood <- 'mv754wp'
    AIDS_inAppearance <- 'mv756'
    CondomOK <- 'mv822'
    HIVTested <- 'mv781'   
    Religion <- 'mv130'
    Food_Purchase <- 'mv825'  
  }
  #
  # Set the Age Band as required - as alternative to the DHS Age Band groupings
  #
  Age_Band_1 <- 19
  Age_Band_2 <- 25
  Age_Band_3 <- 35
  
  print(nrow(IR_dta))
  
  # As features are common across the datasets the processing is the same - sued to select the columns from the
  # DHS datasets
  IR_cols <- c(clusterno, hholdno, rplno, region_no, sample_weight, Act_Age, Age, Location, HHead, Literacy, 
               hasMediaAccess_Paper, hasMediaAccess_Radio, hasMediaAccess_TV, AgeFirstSex, Working,
               Beating_GoingOut, Beating_Child_Neglect, Beating_Argues, Beating_NoSex, Beating_BurntFood, Union,
               Heard_of_AIDS, AIDS_fromMosquitos,AIDS_fromFood,AIDS_inAppearance,CondomOK,HIVTested, Religion, Food_Purchase)
  #
  # A replication of the list above but as string values
  # Used for the column names
  IR_col_names <- c('clusterno', 'hholdno', 'rplno', 'region_no', 'sample_weight','Act_Age', 'Age','Location','HHead','Literacy',
                    'hasMediaAccess_Paper','hasMediaAccess_Radio','hasMediaAccess_TV','AgeFirstSex','Working',
                    'Beating_GoingOut','Beating_Child_Neglect','Beating_Argues','Beating_NoSex','Beating_BurntFood',
                    'Union', 'Heard_of_AIDS','AIDS_fromMosquitos','AIDS_fromFood','AIDS_inAppearance','CondomOK','HIVTested', 'Religion', 'Food_Purchase')
  
  # Cut down the data set for only the cols we need and rename the column metadata
  IR_factor <- subset(IR_dta, select=IR_cols)
  names(IR_factor) <- IR_col_names
  
  
  # Set the region name - Malawi specific 
  # The DHS dataset has splits the region between rural and urban but this is combined here
  IR_factor['region_name'] <- 'Unknown'
  IR_factor[(IR_factor['region_no'] == 1) | (IR_factor['region_no'] == 2), 'region_name'] <-'Chitipa'
  IR_factor[(IR_factor['region_no'] == 3) | (IR_factor['region_no'] == 4), 'region_name'] <-'Karonga'
  IR_factor[(IR_factor['region_no'] == 5) | (IR_factor['region_no'] == 6), 'region_name'] <-'Nkhata Bay'
  IR_factor[(IR_factor['region_no'] == 7) | (IR_factor['region_no'] == 8), 'region_name'] <-'Rumphi'
  IR_factor[(IR_factor['region_no'] == 9) | (IR_factor['region_no'] == 10), 'region_name'] <-'Mzimba'
  IR_factor[(IR_factor['region_no'] == 11) | (IR_factor['region_no'] == 12), 'region_name'] <-'Likoma'
  IR_factor[(IR_factor['region_no'] == 13) | (IR_factor['region_no'] == 14), 'region_name'] <-'Kasungu'
  IR_factor[(IR_factor['region_no'] == 15) | (IR_factor['region_no'] == 16), 'region_name'] <-'Nkhotakota'
  IR_factor[(IR_factor['region_no'] == 17) | (IR_factor['region_no'] == 18), 'region_name'] <-'Ntchisi'
  IR_factor[(IR_factor['region_no'] == 19) | (IR_factor['region_no'] == 20), 'region_name'] <-'Dowa'
  IR_factor[(IR_factor['region_no'] == 21) | (IR_factor['region_no'] == 22), 'region_name'] <-'Salima'
  IR_factor[(IR_factor['region_no'] == 23) | (IR_factor['region_no'] == 24), 'region_name'] <-'Lilongwe'
  IR_factor[(IR_factor['region_no'] == 25) | (IR_factor['region_no'] == 26), 'region_name'] <-'Mchinji'
  IR_factor[(IR_factor['region_no'] == 27) | (IR_factor['region_no'] == 28), 'region_name'] <-'Dedza'
  IR_factor[(IR_factor['region_no'] == 29) | (IR_factor['region_no'] == 30), 'region_name'] <-'Ntcheu'
  IR_factor[(IR_factor['region_no'] == 31) | (IR_factor['region_no'] == 32), 'region_name'] <-'Mangochi'
  IR_factor[(IR_factor['region_no'] == 33) | (IR_factor['region_no'] == 34), 'region_name'] <-'Machinga'
  IR_factor[(IR_factor['region_no'] == 35) | (IR_factor['region_no'] == 36), 'region_name'] <-'Zomba'
  IR_factor[(IR_factor['region_no'] == 37) | (IR_factor['region_no'] == 38), 'region_name'] <-'Chiradzulu'
  IR_factor[(IR_factor['region_no'] == 39) | (IR_factor['region_no'] == 40), 'region_name'] <-'Blantyre'
  IR_factor[(IR_factor['region_no'] == 41) | (IR_factor['region_no'] == 42), 'region_name'] <-'Mwanza'
  IR_factor[(IR_factor['region_no'] == 43) | (IR_factor['region_no'] == 44), 'region_name'] <-'Thyolo'
  IR_factor[(IR_factor['region_no'] == 45) | (IR_factor['region_no'] == 46), 'region_name'] <-'Mulanje'
  IR_factor[(IR_factor['region_no'] == 47) | (IR_factor['region_no'] == 48), 'region_name'] <-'Phalombe'  
  IR_factor[(IR_factor['region_no'] == 49) | (IR_factor['region_no'] == 50), 'region_name'] <-'Chikwawa'
  IR_factor[(IR_factor['region_no'] == 51) | (IR_factor['region_no'] == 52), 'region_name'] <-'Nsanje'
  IR_factor[(IR_factor['region_no'] == 53) | (IR_factor['region_no'] == 54), 'region_name'] <-'Balaka'
  IR_factor[(IR_factor['region_no'] == 55) | (IR_factor['region_no'] == 56), 'region_name'] <-'Neno'
  #
  # Prints to the console missing and unknown values 
  #
  print("Current Age (values):")
  print(sort(unique(IR_factor$Act_Age)))
  print (paste('Removed Actual Age:', nrow(subset(IR_factor, (Act_Age %in% c(8,9,97)))))) # Current Ages go from 15-49
  print("Age at First Sex (values):")
  print(sort(unique(IR_factor$AgeFirstSex)))
  print (paste('Removed No Sex:', nrow(subset(IR_factor, (AgeFirstSex == 0)))))
  print (paste('Removed AgeFirst Sex:', nrow(subset(IR_factor, (AgeFirstSex %in% c(8,9,97)))))) # insonsistent (97) + 8-9 years
  print("Age (values):")
  print(sort(unique(IR_factor$Age)))
  print (paste('Removed Age:', nrow(subset(IR_factor, (Age %in% c(8,9,10)))))) # Ages (5 years bands) go from category 1 to 7
  print("Location (values):")
  print(sort(unique(IR_factor$Location)))
  print (paste('Removed Location:', nrow(subset(IR_factor, (Location > 2)))))
  print("HHead (values):")
  print(sort(unique(IR_factor$HHead)))
  print (paste('Removed HHead:', nrow(subset(IR_factor, (HHead > 2)))))
  print("Literacy (values):")
  print(sort(unique(IR_factor$Literacy)))
  print (paste('Removed Literacy (no card):', nrow(subset(IR_factor, Literacy == 3)))) # Categories go from 0 to 4. 3= no card with required language, 4= blind/visually impaired
  print (paste('Removed Literacy (blind):', nrow(subset(IR_factor, Literacy == 4)))) # Categories go from 0 to 4. 3= no card with required language, 4= blind/visually impaired
  print("Working (values):")
  print(sort(unique(IR_factor$Working)))
  print (paste('Removed Working:', nrow(subset(IR_factor, (Working == 9))))) # Values from 0 to 3
  print("HIVTested (values):")
  print(sort(unique(IR_factor$HIVTested)))
  print (paste('Removed HIVTested:', nrow(subset(IR_factor, (HIVTested > 1)))))
  print("Union (values):")
  print(sort(unique(IR_factor$Union)))
  print (paste('Removed Union:', nrow(subset(IR_factor, (Union == 9))))) # Values from 0 to 2
  print("CondomOK (values):")
  print(sort(unique(IR_factor$CondomOK)))
  print (paste('Removed CondomOK:', nrow(subset(IR_factor, (CondomOK == 9)))))
  
  # Fill all na to 8 - the code will handle these later
  # Avoid using nulls and set to the DHS standard code
  IR_factor[is.na(IR_factor)] <- 8
  
  IR_factor[IR_factor['AgeFirstSex'] == 8 | IR_factor['AgeFirstSex'] == 9 | IR_factor['AgeFirstSex'] == 97, 'AgeFirstSex'] <- NA
  #IR_factor[IR_factor['Literacy'] == 3 | IR_factor['Literacy'] == 9 | IR_factor['Literacy'] == 4, 'Literacy'] <- NA
  IR_factor[IR_factor['Literacy'] == 3 | IR_factor['Literacy'] == 4, 'Literacy'] <- NA
  
  #
  # Count any NAs that will be used by the MICE
  #
  na_count <-sapply(IR_factor, function(y) sum(length(which(is.na(y)))))
  na_count <- data.frame(na_count)
  print(paste('NA before:',na_count))
  #
  # Set up the MICE routine
  #
  init = mice(IR_factor, maxit=0) 
  meth = init$method
  predM = init$predictorMatrix
  #
  # Set up for only those columns that are needed.
  # The mice package does feature extraction which we don't need
  # so it's not done on the whole dataset
  #
  for (each_col in IR_col_names){
    meth[c(each_col)]="" }
  
  meth[c("AgeFirstSex")]="pmm"
  meth[c("Literacy")]="pmm"
  # Five imputations 
  imputed <- mice::mice(IR_factor, m = 5, method = meth, predictorMatrix=predM, seed = 123, printFlag = FALSE)
  print("Imputation data: ")
  print(imputed)
  IR_factor <- complete(imputed)
  
  #
  # Re-check the nulls - any at this stage indicates a problem
  #
  na_count <-sapply(IR_factor, function(y) sum(length(which(is.na(y)))))
  na_count <- data.frame(na_count)
  print(paste('NA after:',na_count))
  #
  # Remove the factors so we can process these manually
  #
  IR_factor[, ] <- lapply(IR_factor[, ], as.character)
  #
  # Make syntatic changes to the DHS encoding
  #
  IR_factor[IR_factor['Age'] == 1, 'Age'] <-'15-19'
  IR_factor[IR_factor['Age'] == 2, 'Age'] <-'20-24'
  IR_factor[IR_factor['Age'] == 3, 'Age'] <-'25-29'
  IR_factor[IR_factor['Age'] == 4, 'Age'] <-'30-34'
  IR_factor[IR_factor['Age'] == 5, 'Age'] <-'35-39'
  IR_factor[IR_factor['Age'] == 6, 'Age'] <-'40-44'
  IR_factor[IR_factor['Age'] == 7, 'Age'] <-'45-49'
  #
  # Location decode
  #
  IR_factor[IR_factor$Location == 1, 'Location'] <-'Urban'
  IR_factor[IR_factor$Location == 2, 'Location'] <-'Rural'
  #
  # Gender of Household head
  #
  IR_factor[IR_factor$HHead == 1, 'HHead'] <-'Male'
  IR_factor[IR_factor$HHead == 2, 'HHead'] <-'Female'
  #
  # Literacy decode
  #
  IR_factor[IR_factor$Literacy == 0, 'Literacy'] <-'Poor Reader'
  IR_factor[IR_factor$Literacy == 1, 'Literacy'] <-'Poor Reader'
  IR_factor[IR_factor$Literacy == 2, 'Literacy'] <-'Good Reader'
  IR_factor[IR_factor$Literacy == 3, 'Literacy'] <-'No card'
  IR_factor[IR_factor$Literacy == 4, 'Literacy'] <-'Poor Reader'
  IR_factor[IR_factor$Literacy == 9, 'Literacy'] <-'Missing'
  #
  # Employment Decode
  #
  IR_factor[IR_factor$Working == 0, 'Working'] <-'Not Working'
  IR_factor[IR_factor$Working == 1, 'Working'] <-'Not Working'
  IR_factor[IR_factor$Working == 2, 'Working'] <-'Working'
  IR_factor[IR_factor$Working == 3, 'Working'] <-'Working'
  IR_factor[IR_factor$Working == 9, 'Working'] <-'Missing'
  #
  # HIV Tested Decode
  #  
  #IR_factor  <- subset(IR_factor, !(HIVTested %in% c(9)))
  IR_factor[IR_factor$HIVTested == 0, 'HIVTested'] <-'Not Tested'
  IR_factor[IR_factor$HIVTested == 1, 'HIVTested'] <-'Tested'
  IR_factor[IR_factor$HIVTested == 9, 'HIVTested'] <-'Missing'
  #
  # Union status Decode
  #
  #IR_factor  <- subset(IR_factor, !(Union %in% c(9)))
  IR_factor[IR_factor$Union == 0, 'Union'] <- 'Never'
  IR_factor[IR_factor$Union == 1, 'Union'] <- 'In Union'
  IR_factor[IR_factor$Union == 2, 'Union'] <- 'Formerly in Union'
  IR_factor[IR_factor$Union == 9, 'Union'] <- 'Missing'
  #
  #  Summarise Media Access, Domestic Violence and AIDS into binary variables
  #
  #
  # Media - If reads paper, listens to radio or watches TV more than once a week then Media Acess = TRUE
  #
  IR_factor$hasMediaAccess <- (IR_factor$hasMediaAccess_Paper == 2 | IR_factor$hasMediaAccess_Paper == 3
                               | IR_factor$hasMediaAccess_Radio == 2 | IR_factor$hasMediaAccess_Radio == 3
                               | IR_factor$hasMediaAccess_TV == 2  | IR_factor$hasMediaAccess_TV == 3)
  #
  # Wife Beating - any form of abuse set this to true
  #
  IR_factor$BeatingsOK <- (IR_factor$Beating_Argues == 1 | IR_factor$Beating_GoingOut == 1 | IR_factor$Beating_Child_Neglect == 1 |
                             IR_factor$Beating_NoSex == 1 | IR_factor$Beating_BurntFood == 1 | 
                             IR_factor$Beating_Argues == 8 | IR_factor$Beating_GoingOut == 8 | IR_factor$Beating_Child_Neglect == 8 |
                             IR_factor$Beating_NoSex == 8 | IR_factor$Beating_BurntFood == 8)
  
  #
  # HIV - Any lack of knowledge sets ignorance - don't know is also set as ignorance
  #
  IR_factor[IR_factor$AIDS_fromMosquitos == 8, 'AIDS_fromMosquitos'] <- 1
  IR_factor[IR_factor$AIDS_fromFood == 8, 'AIDS_fromFood'] <- 1
  IR_factor[IR_factor$AIDS_inAppearance == 8, 'AIDS_inAppearance'] <- 0  # Whether the a person's look shows AIDS is inversed to other questions
  IR_factor$AIDSKnowledgeOK <- (IR_factor$AIDS_fromMosquitos == 0 | IR_factor$AIDS_fromFood == 0 | IR_factor$AIDS_inAppearance == 1)
  #
  # Ok to ask for Condom if STI
  #
  IR_factor$CondomOK = ifelse(IR_factor$CondomOK==0, FALSE, TRUE)
  #
  # Update the booleans to something more descriptive. Espeically useful for MCA
  #
  IR_factor$CondomOK = ifelse(IR_factor$CondomOK==TRUE, 'condom_y', 'condom_n')
  IR_factor$hasMediaAccess = ifelse(IR_factor$hasMediaAccess==TRUE, 'mediaaccess_y', 'mediaaccess_n')
  IR_factor$BeatingsOK = ifelse(IR_factor$BeatingsOK==TRUE, 'beatingsOK_y', 'beatingsOK_n')
  IR_factor$AIDSKnowledgeOK = ifelse(IR_factor$AIDSKnowledgeOK==TRUE, 'AIDS_KnowOK_y', 'AIDS_KnowOK_n')
  #
  # Decode the original data fields from the DHS so it is more descriptive - good for EDA
  #  
  IR_factor[IR_factor$Beating_Argues == 0, 'Beating_Argues'] <-'Not Acceptable'
  IR_factor[IR_factor$Beating_Argues == 1, 'Beating_Argues'] <-'Acceptable'
  IR_factor[IR_factor$Beating_Argues == 8, 'Beating_Argues'] <-'NK'
  IR_factor[IR_factor$Beating_GoingOut == 0, 'Beating_GoingOut'] <-'Not Acceptable'
  IR_factor[IR_factor$Beating_GoingOut == 1, 'Beating_GoingOut'] <-'Acceptable'
  IR_factor[IR_factor$Beating_GoingOut == 8, 'Beating_GoingOut'] <-'NK'
  IR_factor[IR_factor$Beating_Child_Neglect == 0, 'Beating_Child_Neglect'] <-'Not Acceptable'
  IR_factor[IR_factor$Beating_Child_Neglect == 1, 'Beating_Child_Neglect'] <-'Acceptable'
  IR_factor[IR_factor$Beating_Child_Neglect == 8, 'Beating_Child_Neglect'] <-'NK'
  IR_factor[IR_factor$Beating_NoSex == 0, 'Beating_NoSex'] <-'Not Acceptable'
  IR_factor[IR_factor$Beating_NoSex == 1, 'Beating_NoSex'] <-'Acceptable'
  IR_factor[IR_factor$Beating_NoSex == 8, 'Beating_NoSex'] <-'NK'
  IR_factor[IR_factor$Beating_BurntFood == 0, 'Beating_BurntFood'] <-'Not Acceptable'
  IR_factor[IR_factor$Beating_BurntFood == 1, 'Beating_BurntFood'] <-'Acceptable'
  IR_factor[IR_factor$Beating_BurntFood == 8, 'Beating_BurntFood'] <-'NK'
  
  IR_factor[IR_factor$AIDS_fromMosquitos == 0, 'AIDS_fromMosquitos'] <-'No'
  IR_factor[IR_factor$AIDS_fromMosquitos == 1, 'AIDS_fromMosquitos'] <-'Yes'
  IR_factor[IR_factor$AIDS_fromMosquitos == 8, 'AIDS_fromMosquitos'] <-'NK'
  
  IR_factor[IR_factor$AIDS_fromFood == 0, 'AIDS_fromFood'] <-'No'
  IR_factor[IR_factor$AIDS_fromFood == 1, 'AIDS_fromFood'] <-'Yes'
  IR_factor[IR_factor$AIDS_fromFood == 8, 'AIDS_fromFood'] <-'NK'
  
  IR_factor[IR_factor$AIDS_inAppearance == 0, 'AIDS_inAppearance'] <-'No'
  IR_factor[IR_factor$AIDS_inAppearance == 1, 'AIDS_inAppearance'] <-'Yes'
  IR_factor[IR_factor$AIDS_inAppearance == 8, 'AIDS_inAppearance'] <-'NK'
  
  IR_factor[IR_factor$hasMediaAccess_Paper == 0, 'reads_Paper'] <- 'Never'
  IR_factor[IR_factor$hasMediaAccess_Paper == 1, 'reads_Paper'] <- '1 per week'
  IR_factor[IR_factor$hasMediaAccess_Paper == 2, 'reads_Paper'] <- '>1 per week'
  IR_factor[IR_factor$hasMediaAccess_Paper == 3, 'reads_Paper'] <- 'Everyday'
  IR_factor[IR_factor$hasMediaAccess_Paper == 9, 'reads_Paper'] <- 'missing'
  
  IR_factor[IR_factor$hasMediaAccess_Radio == 0, 'listen_Radio'] <- 'Never'
  IR_factor[IR_factor$hasMediaAccess_Radio == 1, 'listen_Radio'] <- '1 per week'
  IR_factor[IR_factor$hasMediaAccess_Radio == 2, 'listen_Radio'] <- '>1 per week'
  IR_factor[IR_factor$hasMediaAccess_Radio == 3, 'listen_Radio'] <- 'Everyday'
  IR_factor[IR_factor$hasMediaAccess_Radio == 9, 'listen_Radio'] <- 'missing'
  
  IR_factor[IR_factor$hasMediaAccess_TV == 0, 'watch_TV'] <- 'Never'
  IR_factor[IR_factor$hasMediaAccess_TV == 1, 'watch_TV'] <- '1 per week'
  IR_factor[IR_factor$hasMediaAccess_TV == 2, 'watch_TV'] <- '>1 per week'
  IR_factor[IR_factor$hasMediaAccess_TV == 3, 'watch_TV'] <- 'Everyday'
  IR_factor[IR_factor$hasMediaAccess_TV == 9, 'watch_TV'] <- 'missing'
  #
  # Set up the manual age band as above
  #
  IR_factor$Act_Age = as.integer(IR_factor$Act_Age)
  IR_factor[IR_factor$Act_Age <= Age_Band_1, 'Age_Band_Manual'] <- paste(Age_Band_1, 'and under')
  IR_factor[(IR_factor$Act_Age > Age_Band_1) & (IR_factor$Act_Age <= Age_Band_2) , 'Age_Band_Manual'] <- paste(Age_Band_1 + 1, 'to', Age_Band_2)
  IR_factor[(IR_factor$Act_Age > Age_Band_2) & (IR_factor$Act_Age <= Age_Band_3) , 'Age_Band_Manual'] <- paste(Age_Band_2 + 1, 'to', Age_Band_3)
  IR_factor[IR_factor$Act_Age > Age_Band_3, 'Age_Band_Manual'] <- paste('Over',Age_Band_3)
  #
  # Discretised Age at First sex but remove - No Sex
  # Use 4 bins which is the same as that requested for the Age Band
  #
  IR_factor  <- subset(IR_factor, !(AgeFirstSex == 0))
  IR_factor$AgeFirstSex = as.integer(IR_factor$AgeFirstSex)
  dis_bins <- discretize_get_bins(data = IR_factor, input = 'AgeFirstSex', n_bins = 4)
  IR_factor <- discretize_df(data=IR_factor, data_bins=dis_bins, stringsAsFactors=T)
  #
  # Religion - not used - left for future work
  #
  IR_factor[(IR_factor$Religion >= 1) & (IR_factor$Religion <= 5), 'Religion'] <- 'Christian'
  IR_factor[(IR_factor$Religion == 7) | (IR_factor$Religion == 96), 'Religion'] <- 'Other'
  IR_factor[(IR_factor$Religion == 6), 'Religion'] <- 'Muslim'
  #
  # AIDS Stigma - not used - left for future work
  #
  IR_factor[(IR_factor$Food_Purchase = 0) | (IR_factor$Food_Purchase = 8), 'HIVStigma'] <- TRUE
  IR_factor[(IR_factor$Food_Purchase == 1), 'HIVStigma'] <- 'FALSE'
  #
  # Sample the data
  #
  sample_weights = as.numeric(IR_factor$sample_weight)/1000000
  
  samp.index <- sample(1:nrow(IR_factor), prob = sample_weights, replace = TRUE)
  IR_factor <- IR_factor[samp.index, ]
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  #
  # Get HIV data
  #
  dta.HIV <- read.dta(file=paste0(basePath, HIV_file), convert.factors = FALSE)
  #
  # Join up to the HIV table
  #  
  # 0  HIV negative,  1  HIV positive, 7 Indeterminate, 9 Inconclusive
  dta.HIV.filter <- subset(dta.HIV, hiv03 == 0 | hiv03 ==1)
  dta.HIV.filter <- subset(dta.HIV.filter, select=c(hivclust, hivnumb, hivline, hiv03))
  names(dta.HIV.filter) <-  c('clusterno', 'hholdno', 'rplno', 'HIVStatus')
  #
  # Join HIV data to main dataset
  #
  cols_HIVjoin = c('clusterno', 'hholdno', 'rplno')
  IR.HIV <- merge(IR_factor,dta.HIV.filter,  cols_HIVjoin, all.x=TRUE)
  IR.HIV[is.na(IR.HIV)] <- 8
  IR.HIV$HIVStatus <- as.factor(IR.HIV$HIVStatus)
  #
  # Output to csv file if requested
  #  
  if (tocsv) {
    filename = paste0(basePath, 'dta_HIV_', gender,'.csv')
    write.csv(IR.HIV, file = filename ,row.names=FALSE)
  }
  
  # All done so return the data set to the calling routine
  return(IR.HIV)
}
