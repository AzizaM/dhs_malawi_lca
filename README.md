# Spatial Analysis of HIV using DHS datasets

## Data
The data is taken from the DHSProgram https://dhsprogram.com/Data/
The code (is) was originally written for Malawi data, hence the file names, but with a few modifications should be suitable for all countries - and different features. 

It is assumed that the data is in a directory that has subfolders for the different DHS datasets e.g.
    - DHSDataset
        + FemaleData
        + MaleData
        + GISData
        + HIV Data
---------------------------------------

## Required R Packages
dplyr,ggplot2 - used for most scripts
foreign, funModeling, mice - used in the data preprocessing 
rgdal, ggmap, png - used for mapping
poLCA - used for clustering
clustrd, factoextra, factoMineR, reshape2 - used for dimensionality reduction

## Code Overview
There is one module dhs_clustering.r that it a set of function calls for clusteringt and other analysis - see **Running the Code** the code. The functions are in separate script files and look at the code for detail on the parameters to be passed.

# Modules
## dhs_clustering.r
This script co-ordinates the functions and has specific steps that should be run as code blocks. It is isn't intended that this script is run in entirity from end to end.

### Initial Set up
1. Set up the working directory to the location of the R source code 
setwd("~/") 
2. Set the base location for the files e.g. basePath = '/home/DHSDataset/'. Ensure that the path name ends with a '/'. If the male, female, HIV data files are in separate directories then this will need to be modified in the dhs_build_dataset.r

### Running the code
The code is broken down into Blocks and the folloeing describs the function of each block.

1. BLOCK 1: 
        - modify the gender depending on the dataset required
        - ensure the correct file and location if the datasets are passed to the function **get_DHS_dta**. This returns the dataset into DHS.main that holds the processed dataset ready for clustering.
        - Set up the minimum and maximum range of clusters to be used when running the LCA algorithm
        The function get_DHS_dta can export the data to a csv file.
2. BLOCK 2: Set up the columns to be used for clustering. Once the dataset has been set up in BLOCK1, this block defines the featuresto be used for clustering. Each variable holds the same columns but in a different way, so each one needs changing:
            **subtitle**: this is a string of the column names so it can be used on as a plot subtitles.
            **LCA_form**: this is the LCA formula passed to the LCA algorithm
            **cols_to_use**: the column names as a list oif strings - this is used to subset the columns in the dataset passed to teh LCA function.
3. BLOCK 3:  This runs teh LCA for a range of clusters and charts the BICs.
            * **run_LCA** (dhs_lca.r) runs LCA for the range of clusters set up in BLOCK 1 in the parameter list.
            * the following lines use ggplot to print the BIC values of each cluster so that the best cluster can be reviewed.
4. BLOCK 4:  Gets the LCA output for the best cluster.
            * Update the **best_cluster** variable to the best cluster from Step 3
            * **get_clusters_LCA** (dhs_lca.r)  gets the output of the poLCA function and $predclass has the class membership.The proportions are output to the console. 
5. BLOCK 5: This an optional step that plots the clusters using dimensionality reduction. 
            - **CACluster** (dhs_helper_functions.r) Runs clustrd::clusmca and prints shows the cluster memebership for each observation.
            - **factoextra::MCA**. This uses the package factoextra to calculate and several plots to analyse the dimenstionality reduction.
6. BLOCK 6: This block creates mapped graphs using rgdal. This was not used for the final Malawi visualisation but is left here as it might be useful code There are two maps:
            - **getclusterlocation**(dhs_helper_functions.r) This links the main data observations file, cluster membership and the geo data
            - **getclusterplots** (dhs_plotmaps.r) This outputs each cluster on the map with bubble size for the number of respondents. Ensure the shape file and path numebr have been updated.
            - **print_malawi_district_clusters** (dhs_plotmaps.r)- This ouptuts to disk a barchart .png for each regioin showing the cluster membership. It insets a map of Malawi with the region highlighted. **cbbPalette** needs to have a colour for each cluster.
7. BLOCK 7: This has several miscellaeneous functions for analysis of the data
            * **plot_correlations** (dhs_helper_functions.r)- plots the correlations of the columns passed in and outputs the p.value as a heatmap from chi.sq to show the independence.
            * **plot_feature_barcharts** (dhs_helper_functions.r) - takes a group of cloumns as a vector and outputs the barcharts for quick EDA.This uses a shape file downloaded from
            * There is some useful fuinctionality to display a particular barchart using ggplot
            * **get_variances** (dhs_helper_functions.r) - this gets the variance of the sum of the probabilites for each column. This can be used for feature elimination
            * **output_cluster_to_disk** (dhs_helper_functions.r) - this is a simple function to write output ot a csv file. The main purpose is for exporting to Excel
 
*** 
SCRIPTS
Possibly the key function as this pre-processes all the data so that it is ready for clustering ana analysis.
## dhs_build_dataset.r
This has one function - get_DHS_dta. 
            * Input parameters
                - gender - 'M' or 'F'
                - basePath - base location for data files
                - to_csv - True/False defaults to False. Output the main data file to a csvb file
                - DHS_file - the name of teh DHS fiel to process
                - HIV_file - the fiel the holds the HIV data
            
1. It may be necessary to change filenames and locations. It is assumed the data is in the stata format.
2. Only certain columns from the DHS dataset are used.
3. Age_Band_1,Age_Band_2,Age_Band_3 can be modified to changed so that age bandings other than those provided by the DHS.
4. To modify columns to be used update the variables IR_cols and IR_col_names. These lists should contain to the same values.
5. The functions creates aggreagations for Media Access, Wife Beating, AIDS Knowledge. It is recommnded these are used for clustering.
6. It decodes the DHS data. Decoding the data allows for easier and quicker EDA.
7. It removes missing values and handles instances when the interviewer has entered unknown.
8. It uses discretize_bins to create four bins for the Age at First Sex
9. It drops respondents who have not had sex. 
10. MICE is used for missing dta values but these need to be added in manually as required.

## dhs_lca.r
This has two functions

1. run_LCA - This runs LCA for each cluster size. The number of repetitions (set to 10) and maximum iterations (set to 10,000) for the LCA function are set here.
2. get_clusters_LCA - This returns a vector of the cluster membership for the cluster size passed to the function.
       
## dhs_helper_functions.r
This has the functions:

1.  getclusterlocation() - This returns a dataset with the main data, the cluster memebership and the respondents longitiude/latitutde. Respondents without co-ordinates are dropped.
2.  cluster_proptest() - this does the two proportion test for each cluster. It prints the output directly to the console. It requires the main data with a column 'cluster' that has the cluster memebership for each row. The output from getclusterlocation() can be used for this.
3.  CACluster () returns a plot of dimensionality reduction
4.  get_variances() - this prints the sum of the probability variance
5.  output_cluster_to_disk() - groups the data by cluster and admin district for output to csv. This is then use din Excel for charting.

## dhs_plot_maps.r
This has the functions:

1. getclusterplots() - For the dataset that has the HIV data and cluster memebership this returns a plot for each cluster of the location of respondents and the levels of HIV. As the DHS dataset granulaity is at a level such that several respondents have the same longitude and latitude. To overcome this the size of the point on the map represents the number of individuals. The points have a transparency so two points with a different HIV status can be seen.
2. createcountrymap() - this functions takes the location of the country shape file and using readOGR() and fortify() returns a dataframe that can be used in ggplot. _For countries other than Malawi this function will need to altered for the number regions (see countrymap.df$id in the code)_ 
3. printmap() - This takes the country map, the data with the cluster memebership,longitude/latitude, the cluster number and a variable to include information on the plot titles (e.g. gender). It returns the plot.
4. print_malawi_district_clusters() - prints graphs to disk so they can be assemebled into a visualistion. There are two types of ouptut: 
    - Prints the district of Malawi that is overlayed onto the main barchart.
    - Main barchart showing the cluster breakdown.