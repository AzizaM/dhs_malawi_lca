#================================================================================================================================
# MIT LICENCE
# Copyright (c) 2018, Amanda Styles
#  
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation 
#  the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
#  and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#  
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
#   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
#   IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
#   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
#   OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
#================================================================================================================================
library(poLCA)
library(clustrd)
###########################################################################################################################
#
#  LCA as a function for multiple unattended runs
#
##########################################################################################################################
#
# Set up the data - for LCA this all the data in the dataframe. This outputs the BIC value and other measures for each cluster
# INPUT PARAMETERS
#  dta - the dataset with all the observations
#  LCA_cols - a list of strings that has the columns to be used for clustering
#  LCA_form - the formula to be passed to the LCA algorithm
#  min_clusters and max_clusters - the range of clusters
#
# OUTPUT PARAMETERS
#  Returns a dataframe with the BIC (and other output) for each cluster
#
run_LCA <- function(dta, LCA_cols, LCA_form, min_clusters = 2, max_clusters = 10) {
  #
  # The routine takes the whole dataset - this first part restricts to the columns required
  #
  samp.IR <- dta
  samp.IR <- as.data.frame(unclass(samp.IR))
  clustercols <- LCA_cols
  dta.cluster <- samp.IR[ , (names(samp.IR) %in% clustercols)]
  #
  #
  # Initialise the vectors, LCA Formula
  #
  set.seed(456)
  
  form <- LCA_form
  
  cluster_size <- numeric(max_clusters)
  BIC <- numeric(max_clusters)
  resid.DF <- numeric(max_clusters)
  LLOGL <- numeric(max_clusters)
  LLRATIO <- numeric(max_clusters)
  clusterlist <- c(max_clusters)
  
  #
  # Run the LCA for each cluster adn create vectors for output
  #
  count <- 0 
  for (i in min_clusters:max_clusters) {
    
    print(paste('Starting Cluster',i))
    res_lca <- poLCA(form, dta.cluster, maxiter = 10000, nclass = i, nrep = 10, verbose = TRUE)
    
    count <- count + 1  
    cluster_size[count] <- i
    BIC[count] <- res_lca$bic
    resid.DF[count] <- res_lca$resid.df
    LLOGL[count] <- res_lca$llik
    LLRATIO[count] <- res_lca$Gsq
    
    print(paste('Cluster',i,'complete.'))
  }
  #
  # Add all the lists above into a dataframe
  #
  dta.lca <- data.frame(cluster_size, BIC, resid.DF, LLOGL, LLRATIO, stringsAsFactors=FALSE)
  dta.lca.subset <- subset(dta.lca, dta.lca$cluster_size > 0)
  
  return (dta.lca.subset)
}
###########################################################################################################################
#
#  Returns the cluster membership for each row the best_cluster
#
# INPUT PARAMETERS
#  dta - the dataset with all the observations
#  LCA_cols - a list of strings that has the columns to be used for clustering
#  LCA_form - the formula to be passed to the LCA algorithm
#  best_cluster - the cluster size to be used
#
# OUTPUT PARAMETERS
#  Returns the output from the cluster - see documentation for poLCA::poLCA for more detail
get_clusters_LCA <- function(dta, LCA_cols, LCA_form, best_cluster) {
  #
  # This essentially re-runs for one cluster but returns the cluster output so the 
  # class memebership can be extracted
  #
  samp.IR <- dta
  samp.IR <- as.data.frame(unclass(samp.IR))
  
  clustercols <- LCA_cols
  dta.cluster <- samp.IR[ , (names(samp.IR) %in% clustercols)]
  
  set.seed(456)
  
  print(paste('Starting Cluster...'))
  
  res_lca <- poLCA(LCA_form, dta.cluster, maxiter = 10000, nclass = best_cluster, nrep = 10, verbose = TRUE)
  
  print(paste('Cluster complete.'))
  
  return (res_lca)
}
